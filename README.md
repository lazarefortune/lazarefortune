![](https://komarev.com/ghpvc/?username=lazarefortune)

## Hi I'm Lazare Fortune 👋

I'm Lazare Fortune, a junior backend developer based in Paris, France.

### Social 📱
---
You can find me on the following social media plateforms or send me an email :
- :computer: [Portfolio](https://www.lazarefortune.com)
- :movie_camera: [YouTube](https://www.youtube.com/channel/UCITKwfT7qVXjdHHu84Atodw)
- :technologist: [LinkedIn](https://www.linkedin.com/in/lazare-fortune/)
- :earth_africa: [Twitter](https://twitter.com/lazarefortune)
- :envelope: [lazarefortune@gmail.com](lazarefortune@gmail.com)

### Note
---
- 🔭 I’m currently working on Node Js, Next Js and Symfony
- 👯 I’m looking to collaborate on Node Js

<!--
**lazarefortune/lazarefortune** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on Node Js, Next Js and Symfony
- 🌱 I’m currently learning Symfony and Node Js
- 👯 I’m looking to collaborate on Symfony
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

[![trophy](https://github-profile-trophy.vercel.app/?username=lazarefortune)](https://github.com/ryo-ma/github-profile-trophy)

### GitHub Analytics

<p align='center'>
  <a href="https://github.com/lazarefortune">
    <img height="180em" width="49%" src="https://github-readme-stats-eight-theta.vercel.app/api?username=lazarefortune&show_icons=true&theme=dracula" />
    <img height="180em" width="49%" src="https://github-readme-stats-eight-theta.vercel.app/api/top-langs/?username=lazarefortune&layout=compact&langs_count=8&theme=dracula"/>
  </a>
</p>

[![Ashutosh's github activity graph](https://activity-graph.herokuapp.com/graph?username=David-Moisan&theme=dracula)](https://github.com/ashutosh00710/github-readme-activity-graph)
